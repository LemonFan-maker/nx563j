/* This file is auto generated, version 1 */
/* SMP PREEMPT */
#define UTS_MACHINE "aarch64"
#define UTS_VERSION "#1 SMP PREEMPT Tue Nov 14 14:59:15 UTC 2023"
#define LINUX_COMPILE_BY "lemonfan-maker"
#define LINUX_COMPILE_HOST "Github-Action"
#define LINUX_COMPILER "Android (8490178, based on r450784d) clang version 14.0.6 (https://android.googlesource.com/toolchain/llvm-project 4c603efb0cca074e9238af8b4106c30add4418f6)"
